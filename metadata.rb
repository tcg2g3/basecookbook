name             "basecookbook"
maintainer       "G2G3 digital"
maintainer_email "it@g2g3.digital"
license          "All rights reserved"
description      "Functional Testing of Policies, Precedence and Wrapper Cookbooks  for G2G3 Digital"
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version          "0.0.01"
